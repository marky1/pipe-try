package pipe_try

type pipeTry struct {
	err error
}
func Init(fn func()(interface{},error),out interface{}) *pipeTry {
	return Empty().Pipe(fn,out)
}
func Empty() *pipeTry {
	return &pipeTry{nil}
}
func (t *pipeTry)hasError() bool{
	return t.err!=nil
}
func (t *pipeTry) Pipe(fn func()(interface{},error),out interface{}) *pipeTry {
	if t.hasError() {
		return t
	}
	out,err:=fn()
	t.err=err
	return t
}
func  (t *pipeTry)OnError(fn func(err error)) {
	fn(t.err)
}
func (t *pipeTry)Retry(attempt int,fn func()(interface{},error),out interface{}) *pipeTry {

	if t.hasError(){
		return t
	}
	for attempt>0{
		v,err:=fn()

		t.err=err
		if !t.hasError(){
			out=v
			return t
		}
		attempt--
	}
	return t
}

