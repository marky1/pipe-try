package pipe_try

import (
	"testing"
	"errors"
)

func TestEmptyFunc(t *testing.T) {
	if Empty().err!=nil{
		t.Fatal("Empty's err should be nil")
	}
}
func TestPipeFunc(t *testing.T) {
	var isSuccess bool
	err:=errors.New("err1")
	ptry:=Empty().Pipe(func() (interface{}, error) {
		return false ,err
	},isSuccess).Pipe(func() (interface{}, error) {
		return true,nil
	},isSuccess)
	if ptry.err==nil{
		t.Fatal("err shouldn't be nil.")
	}
	if isSuccess{
		t.Fatal("isSuccess should be false.")
	}

}